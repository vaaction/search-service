package org.ibukanov.search.model

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.{DefaultJsonProtocol, PrettyPrinter}

case class TagStatistics(total: Int, answered: Int) {
  def + (t: TagStatistics) = {
    TagStatistics(total + t.total, answered + t.answered)
  }
}
case class TagData(tag: String, statistics: TagStatistics)

case class SearchResultResponse(tagsData: Map[String, TagStatistics]) {
  def this(stackExchangeSearchResults: Seq[StackExchangeSearchResult]) = this {
    stackExchangeSearchResults.flatMap(_.items)
      .flatMap(i => i.tags.map(t => TagData(t, TagStatistics(1, i.answer_count))))
      .groupBy(_.tag).mapValues(s => s.map(_.statistics).reduceLeft(_ + _))
  }
}

trait SearchResultResponseJsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val printer = PrettyPrinter
  implicit val itemFormat = jsonFormat2(TagStatistics)
}