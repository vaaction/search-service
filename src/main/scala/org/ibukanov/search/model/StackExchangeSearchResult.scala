package org.ibukanov.search.model

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol

case class Item(tags: List[String], answer_count: Int)
case class StackExchangeSearchResult(items: List[Item])

trait StackExchangeSearchResultJsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val itemFormat = jsonFormat2(Item)
  implicit val searchResult = jsonFormat1(StackExchangeSearchResult)
}