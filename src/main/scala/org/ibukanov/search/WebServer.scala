package org.ibukanov.search

import akka.actor.ActorSystem
import akka.event.Logging
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer

object WebServer {
  def main(args: Array[String]) {

    val HOST = "localhost"
    val PORT = 8080
    val API_HOST = "api.stackexchange.com"
    val API_SEARCH_URL = "/2.2/search?pagesize=100&order=desc&sort=creation&site=stackoverflow&tagged="

    implicit val system = ActorSystem("search-server-system")
    implicit val materializer = ActorMaterializer()

    val log = Logging.getLogger(system, this)

    val client = new StackExchangeClient(API_HOST, API_SEARCH_URL)
    val route = new Routes(client).mainRoute

    Http().bindAndHandle(route, HOST, PORT)
    log.info(s"Server online at http://$HOST:$PORT")
  }
}