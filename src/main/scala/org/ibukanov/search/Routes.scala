package org.ibukanov.search

import akka.actor.ActorSystem
import akka.http.scaladsl.server.Directives.{complete, get, parameters, path, _}
import akka.stream.ActorMaterializer
import org.ibukanov.search.model.{SearchResultResponse, SearchResultResponseJsonSupport}

import scala.concurrent.Future

class Routes(client: StackExchangeClient)(implicit val materializer: ActorMaterializer, implicit val system: ActorSystem)
  extends SearchResultResponseJsonSupport {

  implicit val executionContext = system.dispatcher

  def mainRoute =
    path("search") {
      get {
        parameters("tag".as[String].*) { tags =>
          val stackExchangeSearchResults = Future.sequence(tags.map(client.search))
          val response = stackExchangeSearchResults.map(r => new SearchResultResponse(r.toSeq).tagsData)
          complete(response)
        }
      }
    }
}
