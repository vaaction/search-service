package org.ibukanov.search

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.coding.{Deflate, Gzip, NoCoding}
import akka.http.scaladsl.model.headers.HttpEncodings
import akka.http.scaladsl.model.{HttpMethods, HttpRequest, HttpResponse}
import akka.http.scaladsl.unmarshalling.{Unmarshal, Unmarshaller}
import akka.stream.{ActorMaterializer, OverflowStrategy, QueueOfferResult}
import akka.stream.scaladsl.{Keep, Sink, Source}
import org.ibukanov.search.model.{StackExchangeSearchResult, StackExchangeSearchResultJsonSupport}

import scala.concurrent.{Future, Promise}
import scala.util.{Failure, Success}


class StackExchangeClient(host: String, searchApiUrl: String, queueSize: Int = 10000)
                         (implicit val system: ActorSystem, implicit val materializer: ActorMaterializer)
  extends StackExchangeSearchResultJsonSupport {

  import system.dispatcher

  private val poolClientFlow = Http().cachedHostConnectionPoolHttps[Promise[HttpResponse]](host)
  private val queue =
    Source.queue[(HttpRequest, Promise[HttpResponse])](queueSize, OverflowStrategy.dropNew)
      .via(poolClientFlow)
      .toMat(Sink.foreach({
        case ((Success(resp), p)) => p.success(resp)
        case ((Failure(e), p))    => p.failure(e)
      }))(Keep.left)
      .run()

  def search(tag: String): Future[StackExchangeSearchResult] = handleRequest(mkRequest(tag))

  private def handleRequest(request: HttpRequest)(implicit um: Unmarshaller[HttpResponse, StackExchangeSearchResult]) = {
    val responsePromise = Promise[HttpResponse]()
    queue.offer(request -> responsePromise).flatMap {
      case QueueOfferResult.Enqueued    => responsePromise.future
      case QueueOfferResult.Dropped     => Future.failed(new RuntimeException("Queue overflowed. Try again later."))
      case QueueOfferResult.Failure(ex) => Future.failed(ex)
      case QueueOfferResult.QueueClosed =>
        Future.failed(new RuntimeException("Queue was closed (pool shut down) while running the request. Try again later."))
    }.map(decodeResponse)
      .flatMap(r => Unmarshal(r).to[StackExchangeSearchResult])
  }

  private def mkRequest(tag: String): HttpRequest = HttpRequest(HttpMethods.GET, s"$searchApiUrl$tag")

  private def decodeResponse(response: HttpResponse): HttpResponse = {
    val decoder = response.encoding match {
      case HttpEncodings.gzip ⇒
        Gzip
      case HttpEncodings.deflate ⇒
        Deflate
      case _ ⇒
        NoCoding
    }
    decoder.decodeMessage(response)
  }
}
